---
title: About me
comments: false
---

On my spare time I contribute [Freedesktop SDK](https://freedesktop-sdk.io/)
and [GNOME OS](https://os.gnome.org/).

### Background

 - Studied at [ÉPITA Paris, France](https://www.epita.fr/en/) until 2005. Member of [LRDE](https://www.lrde.epita.fr/).

 - Did my Ph.D at the [University of Bergen,
   Norway](https://www.uib.no/en) in computer science.

 - Worked for [VizRT](https://vizrt.com/) as software developer.

 - Worked for [Codethink, Ltd](https://codethink.co.uk/) as software engineer.

 - Currently working for [Canonical](https://canonical.com/) as software engineer on Ubuntu Core.

 - I am a GNOME Foundation member.

### Hobbies

 Boardgames, video games, photography, guitar, drums, piano.
