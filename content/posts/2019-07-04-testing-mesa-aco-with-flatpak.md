---
title: Testing Mesa ACO with Flatpak
date: 2019-07-04
tags: ["flatpak", "freedesktop-sdk"]
---

{{% note warning %}}
The Flatpak application for Steam has been updated to use Freedesktop
19.08. The instructions here are deprecated. Please read the updated
instructions in
[the corrected post]({{< ref "2019-09-12-testing-mesa-aco-with-flatpak-updated.md" >}}).
{{% /note %}}

Valve has been [asking users to help with
testing](https://steamcommunity.com/games/221410/announcements/detail/1602634609636894200)
the [ACO
patches](https://github.com/daniel-schuermann/mesa/commits/master) for
[Mesa](http://mesa.freedesktop.org/). It replaces LLVM to compile
shaders.

Testing a development or patched version of Mesa is not necessarily
easy. So in order to help users in doing testing we are building an
extension for the upcoming Freedesktop SDK 19.08. No need to build
anything. Just install and define `FLATPAK_GL_DRIVERS` to select the
patched version of Mesa.

And good news: the [Flatpak application for
Steam](https://github.com/flathub/com.valvesoftware.Steam) has now a
beta release working with 19.08beta of Freedesktop SDK.

However we do not deliver the ACO+Mesa extension to flathub. So you
will need to get it from our development release server.

So here is the procedure. First install the runtime and the mesa
extensions:

```
flatpak remote-add --user freedesktop-sdk https://cache.sdk.freedesktop.org/freedesktop-sdk.flatpakrepo
flatpak install --user freedesktop-sdk \
    org.freedesktop.Platform//19.08beta \
    org.freedesktop.Platform.GL.mesa-aco//19.08beta \
    org.freedesktop.Platform.GL32.mesa-aco//19.08beta \
    org.freedesktop.Platform.GL32.default//19.08beta \
    org.freedesktop.Platform.Compat.i386//19.08beta
```

Then install Steam's beta Flatpak app (it is not the beta of Steam,
just the Flatpak app).

```
flatpak remote-add --user flathub-beta https://dl.flathub.org/beta-repo/flathub-beta.flatpakrepo
flatpak install --user flathub-beta com.valvesoftware.Steam//beta
```

To run it, make sure Steam is not already running. Then:

```
FLATPAK_GL_DRIVERS=mesa-aco flatpak run com.valvesoftware.Steam//beta
```

You should see somewhere in the console the following message:
```
WARNING: Experimental compiler backend enabled. Here be dragons! Incorrect rendering, GPU hangs and/or resets are likely
```

If you see it, you are all set. And you can start testing. Remember
you can set `RADV_PERFTEST=llvm` as environment variable to disable
ACO if you want to compare.

Please follow the [ACO testing
instructions](https://steamcommunity.com/app/221410/discussions/0/1640915206474070669/).

If you are using flatpak to test, please report issues to us before,
either on IRC (`#freedesktop-sdk` on Freenode) or on [our issue
tracker](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues).
We can make sure the issue is not an issue on our side.
