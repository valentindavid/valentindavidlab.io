---
title: Freedesktop SDK OCI/Docker images
date: 2019-06-18
draft: true
tags: ["freedesktop-sdk", "oci"]
---

[bst-external](https://gitlab.com/BuildStream/bst-external) is the
BuildStream plugin registry that is used by Freedesktop SDK.  It
received a new experimental element plugin called `oci`.  This plugin
can produce OCI as well as Docker images.  It should follow the [OCI
image format specifications
1.0.1](https://github.com/opencontainers/image-spec/blob/v1.0.1/spec.md)
and the [Docker image specifications
1.2.0](https://github.com/moby/moby/blob/master/image/spec/v1.2.md).

We have started using it to create images of the Freedesktop
SDK. Those are not published yet, but will hopefully be for the 19.08
release.

### Motivation

Docker and Podman/Buildah can be used to create and run containers.
Container images are created with a `Dockerfile`. This file contains a
list of instructions, mostly shell commmands, that build the
image. Often a distribution is imported, then the distribution package
manager is used to installed dependencies, and maybe some parts are
compiled.

`Dockerfile` are specific to Docker. While it is possible to extract
the image to be imported into another container system, or eventually
modified to be bootable, it requires steps outside of the `Dockerfile`.
It is not made to be used for other purpose than Docker (or Podman).

BuildStream is a generic tool for integrating software stack. It can
be used to build any kind of images and is agnostic to the output
format. It can be used for anything, as long as you provide the
plugin.

For the moment we have been using BuildStream to build Flatpak images
of Freedesktop SDK. Snap images are also coming for the next
release. Some are also working on bootable VM images in order to test
Desktop environments. One runtime, multiple uses.

OCI/Docker images are just one more output format.

Freedesktop SDK aims to be a reference runtime for desktop Linux
applications. If you build an application for it, you should be able
to ship to most users without rebuilding it. So Freedesktop SDK's
runtime has to be able to run anywhere.

Docker is not really meant for desktop. But there are use cases where
code is shared between backend services and applications. Think of
video games with dedicated server and integrated server for
example. So Docker images would allow delivering backend services of
an application along with the front-end in one single dependency
graph.

There is no package manager in Freedesktop SDK. We do not need it
because we just compose and integrate images directly into
BuildStream. And for that reason, there is no point using a `Dockerfile`.

It is possible to import an image to Docker or Podman by providing
a simple tape archive file (`.tar`). But this does not allow
layering.

Layering can be useful to share files between images. In order
to achieve layering, we need a BuildStream plugin capable
of generating multiple Docker/OCI images with common layers.

### Overview

For each image file to generate, a BuildStream element of kind `oci`
need to be created.

The type of image can be selected `mode` configuration entry, values are
`oci` and `docker`.

Each image file may effectively contain multiple images.
So there will be a list of images. Usually, there will be only one image.

This image will contain a configuration. The format of the
configuration will depend on of the "mode" (whether `oci` or
`docker`).  All the details of what is configurable is available in
the [oci plugin
documentation](https://buildstream.gitlab.io/bst-external/elements/oci.html)

### Layers

OCI/Docker images are composed of layers. Typically every command in a
`Dockerfile` will create a layer. A layer is tape archive file
(`.tar`) containing the difference between layers as union filesystem.

Union filesystems are filesystems like OverlayFS, AUFS, UnionFS, which
overlay one root on top of another root. New files from a layer will
be just overlayed. If a layer need to delete a file from a lower
layer, it will add a special whiteout file, which is a file with a
special name.

[See Wikipedia on this
topic](https://en.wikipedia.org/wiki/Union_mount).

There are two important fields in the image configuration: the `parent`
and the `layer`. Both are dependencies. The `parent` must be
Docker or OCI image while the `layer` can be any artifact.

The `parent` Docker image will be extracted from the layers. The
difference from this extracted image to the `layer` artifact is
calculated.  This will create a new layer (a `.tar` file). to be added
on top of the `parent` stack of layers.

Because the difference is calculated, if files from the the `parent`
image are not in the `layer`, then this new layer will contain
whiteouts removing those files. So it is important to remember to
include all the files in the `layer` artifact.

It is possible to not provide a parent image. In this case the
resulting image will have only one layer. The same as `docker import`.

The parent does not have to be the same "`mode`". Layers can be
imported from a Docker image when building and OCI image, or the other
way around.

It is also possible to make an image by just copying layers from
another image and not adding any new layer. This is useful to change
configuration or convert between OCI and Docker.

In theory OCI or Docker images that were not built with BuildStream
could be imported. This could be useful to add new layers to an
existing image without flattening it. However we do not provide any
way to unpack the layers into a flat artifact into BuildStream
yet. And this is needed to build on top of the image. So a new plugin
would be needed to do that, or external image would need to unpacked
it externally and imported in parallel into BuildStream. Because
Freedesktop SDK is not built from a Docker image, we do not need this.

### Designing layers

For every layer needed in an image, an intermediate image element
using the `oci` plugin will be written. Usually the OCI configuration
of those intermediate layers are succint. Only the information for
the "commit" to be added in history will be needed. Which is mostly
the author and the commit message.

In order to optimize disk space and bandwith, layers of common files
have to be created so they can be reused between applications.

For the moment we have created two images: "bootstrap" and "platform".
"Bootstrap" is the minimal image you need to run a build in
BuildStream. In the case of Docker, it makes sense that some images
will depend only on this minimal image. Backend services of an
application that are expected to run on Docker will probably not need
all the desktop dependencies, like fonts or GPU related libraries.

Since "bootstrap" is an intermediate image not expected to
be pushed on a OCI or Docker registry, there is only an OCI
element. We can import that OCI element as parent to Docker images.

```yaml
kind: oci

depends:
- filename: oci/layers/bootstrap.bst
  type: build

config:
  mode: oci
  images:
  - os: linux
    architecture: "%{go-arch}"
    layer:
    - oci/layers/bootstrap.bst
    comment: "Import bootstrap layer from Freedesktop SDK"
```

Then "platform" is created on top of it. This is a container image, so
it contains mostly the same files as for `org.freedesktop.Platform`
for Flatpak or the upcoming Snap images.

```yaml
kind: oci

depends:
- filename: oci/bootstrap-oci.bst
  type: build
- filename: oci/layers/platform.bst
  type: build

config:
  mode: oci
  images:
  - os: linux
    architecture: "%{go-arch}"
    parent:
      element: oci/bootstrap-oci.bst
    layer:
    - oci/layers/platform.bst
    comment: "Import platform layer from Freedesktop SDK"
```

You can find those files [in the source code of Freedesktop
SDK](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/tree/0c8c46c00dffc28a424f1c10184a0781551fedeb/elements/oci).

We do not yet provide debug or development files in images. Since
those are not useful yet in a OCI or Docker context. But we could
create images with those files stored in other layers. That would
allow downloading only the new layers with development and debug
when downloading the development or debug images.

We expect GNOME to provide their own OCI image using layers from
Freedesktop SDK.

### Why not automatically generate layers?

BuildStream builds elements as a graph. This allows building elements
in parallel for example, or knowing the exact dependencies of an
element when extracting it to be used in a different context.

Unfortunately, it is not possible to have a stable topological sort to
automatically generate layers. Elements might need to be overlayed on
top of elements it does not depend on. Sometimes there is a directory,
sometimes a symbolic link to another directory. If there are no
overlap and no symbolic link, this could be fine.

While DiffIDs of layers do not depend on layer ordering, ChainIDs
do. Implementations might index layer checkouts by ChainID because it
is not useful to use DiffIDs in the normal use case. This would then
not save disk space. I believe this is the case currently with Docker.

BuildStream has the notion of split domains. Files can be categorized
as development files or debug files for example. Those are typically
removed in the end image. There are sensible default split domains,
but this is something users can define. And final images might contain
different sets of domains.

We do not want to generate layers with all the domains. The debug
files are typically considerably bigger than the runtime. Keeping them
in all layers to remove them in the last layer would waste disk space
and bandwidth. Each domain would need its own stack of layers
to avoid having files in multiple layers.

While this is possible, to generate automatically layers correctly
might give too many of them.  In Freedesktop SDK image we have around
340 elements. We use around 8 domains. So we would be well over 2000
layers.

Files may live multiple domains which means there would be duplication
of files anyway.

### Aknowledgements

This work has been sponsored by [Codethink Ltd](https://codethink.co.uk/).

{{< codethink-logo >}}
