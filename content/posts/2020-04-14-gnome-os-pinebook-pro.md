---
title: GNOME OS on Pinebook Pro
date: 2020-04-14
tags: ["gnome", "gnome-os", "arm"]
---

Recently, I have been working on running GNOME OS on the Pinebook Pro.

GNOME OS is a bootable image used to test vanilla GNOME without
dependencies on distributions. It is upgradable through OSTree and has
Flatpak to allow installation of applications. GNOME OS is built using
[BuildStream](https://buildstream.build/). Basic dependencies are
provided by [Freedesktop
SDK](https://gitlab.com/freedesktop-sdk/freedesktop-sdk).

The [Pinebook Pro](https://wiki.pine64.org/index.php/Pinebook_Pro) is
a $200 arm 64 laptop from PINE64.

The merge request I am working on is available at:
https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/577

![GNOME Initial Setup running Pinebook Pro](/gnome-os-pinebook-pro.jpg)

U-Boot and the Linux kernel are not yet mainline. The source of the
linux kernel we use for the moment is the branch for Manjaro's build
maintained by [tsys](https://gitlab.manjaro.org/tsys/). U-Boot is
built from a branch maintained by [ENOSPC](https://eno.space/blog/).

Apart from that we mostly use all the components of a standard GNOME
OS image, including Wayland and EFI boot through systemd-boot.

### How to test it

First, note that this is a work in progress, so do not open issues
before the branch is merged. Instead, you may comment on the [merge
request](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/577). But
do not expect everything to work.
(Edit: You can also join `#gnome-os` on GIMPNet)

To download the image, find the latest [pipeline for the merge
request](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/577/pipelines),
where the last bullet is green. On that bullet, select
`pinebook-pro-image`. Then download the job artifact.

In that job artifact, there will be a file named `disk.qcow2`. You can
flash it to an sd card or an eMMC module using `qemu-img dd
if=disk.qcow2 of=/dev/device-to-which-to-write`.

If you do not get anything to boot, I recommend you use the [serial
port of the
computer](https://wiki.pine64.org/index.php/Pinebook_Pro#Using_the_UART)
to debug the issue.

### Updating the keyboard and touchpad firmware

Some first batches of the Pinebook Pro require a firmware
update. Unfortunately, there is no automatic way to do it at the
moment.

If your keyboard is an ISO keyboard, do the following.

```sh
$ sudo pinebook-pro-keyboard-updater step-1 iso
```

Then reboot. After reboot:

```sh
$ sudo pinebook-pro-keyboard-updater step-2 iso
```

Then reboot one more time. The firmware is updated.

If you have an ANSI keyboard, replace `iso` by `ansi` in the command
line.

### Aknowledgements

This work has been sponsored by [Codethink Ltd](https://codethink.co.uk/).

{{< codethink-logo >}}
